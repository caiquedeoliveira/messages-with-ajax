const botaoEnviar = document.querySelector(".botao-enviar")
const url = "http://treinamento-ajax-api.herokuapp.com/messages/"

// Pega o histórico de mensagens
const msgHist = document.querySelector(".mensagens")

botaoEnviar.addEventListener("click", () => {
    const messageInput = document.querySelector(".texto")
    
    const body = {
        message: {
            content: messageInput.value,
            author: "Caique Rodrigues"
        }
    }

    const config = {
        method: "POST",
        body: JSON.stringify(body),
        headers: { "Content-Type": "application/json"}
    }

    fetch(url, config)
    .then(response => response.json())
    .then(message => {
        createPostDiv(message)
    })
    .catch((err) => {
        console.log(err)
    })

    messageInput.value = ""

})

function createPostDiv(message){
        // Pega o texto da caixa
        const li = document.createElement("li")
        li.className = "mensagem"
        li.innerHTML = `
                <h1>${message.author}</h1>
                <p class="texto-msg">${message.content}</p>
                <div class="botoes">
                    <input type="button" value="Editar" class="Editar botao-msg" onclick="editar(this, ${message.id})")>
                    <input type="button" value="Deletar" class="Deletar botao-msg" onclick="deletar(this, ${message.id})")>
                </div>
                <div class="botoes-modo-editar" hidden>
                    <input type="text">
                    <input type="button" value="Enviar" class="Enviar botao-msg" onclick="editarTexto(this, ${message.id})")>
                </div>`
    
        msgHist.appendChild(li)
}

function deletar(elemento, id) {
    
    const config = {
        method: "DELETE"
    }
    
    fetch(url + id, config)
    .then(() => {
        elemento.parentNode.parentNode.remove()
    })
    .catch((error) => {
        console.log(error)
    })
}

function editar(elemento) {
    const parent = elemento.parentNode.parentNode

    // Esconder os botões normais
    parent.querySelector(".botoes").toggleAttribute("hidden")

    // Mostrar os botões de edição
    parent.querySelector(".botoes-modo-editar").toggleAttribute("hidden")
}

function editarTexto(elemento, id) {

    const parent = elemento.parentNode.parentNode
    const textArea = elemento.parentNode.children[0]

    // Editar o texto da mensagem
    parent.querySelector(".texto-msg").innerText = textArea.value

    // Esconder os elementos de editar novamente
    parent.querySelector(".botoes").toggleAttribute("hidden")

    // Tirar o atributo hidden dos botões Editar e Deletar
    parent.querySelector(".botoes-modo-editar").toggleAttribute("hidden")


    const body = {
        message: {
            content: textArea.value
        }
    }

    const config = {
        method: "PUT",
        body: JSON.stringify(body),
        headers: {
            "Content-Type": "application/json"
        }
    }

    fetch(url + id, config)
    .then((response) => response.json())
    .then((message) => {
        console.log(message)
    })
    .catch((error) => {
        console.log(error)
    })

}

const getMessages = () => {
    fetch(url)
    .then(response => response.json())
    .then(messages => {
        messages.forEach(message => {
            createPostDiv(message)
        });
    })
    .catch((err) => {
        console.log(err)
    })
}

getMessages()


